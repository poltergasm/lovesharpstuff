/*
 * LoveSharp port of TLfres for LÖVE
 */
 
using System;

class TLfres
{
    private Love.Vector2 lastMousePos = new Love.Vector2(0, 0);
    private float[] currentlyRendering = new float[2];
    private bool isRendering = false;
    private Love.Vector4 _black = new Love.Vector4(0, 0, 0, 1);

    private Love.Vector2 _GetRawMousePos(int width, int height)
    {
        Love.Vector2 mousePos = Love.Mouse.GetPosition();
        Love.Vector2 mode = new Love.Vector2(Love.Graphics.GetWidth(), Love.Graphics.GetHeight());
        float scale = Math.Min(mode.X/width, mode.Y/height);
        return new Love.Vector2(
            (mousePos.X - (mode.X - width * scale) * 0.5f)/scale,
            (mousePos.Y - (mode.Y - height * scale) * 0.5f)/scale
        );
    }

    public Love.Vector2 GetMousePosition(int width, int height)
    {
        Love.Vector2 mousePos = _GetRawMousePos(width, height);
        if (mousePos.X >= 0 && mousePos.Y <= width && mousePos.Y >= 0 && mousePos.Y <= height) {
            lastMousePos = _GetRawMousePos(width, height);
        }

        return lastMousePos;
    }

    public float GetScale()
    {
        float width = currentlyRendering[0];
        float height = currentlyRendering[1];
        Love.Vector2 mode = new Love.Vector2(Love.Graphics.GetWidth(), Love.Graphics.GetHeight());
        return Math.Min(mode.X/width, mode.Y/height);
    }

    public float GetScale(float width, float height)
    {
        Love.Vector2 mode = new Love.Vector2(Love.Graphics.GetWidth(), Love.Graphics.GetHeight());
        return Math.Min(mode.X/width, mode.Y/height);
    }

    public float BeginRendering(float width, float height, bool centered)
    {
        if (isRendering)
            throw new Exception("Must call EndRendering before calling BeginRendering");
        
        isRendering = true;
        currentlyRendering[0] = width;
        currentlyRendering[1] = height;
        Love.Graphics.Push();

        Love.Vector2 mode = new Love.Vector2(Love.Graphics.GetWidth(), Love.Graphics.GetHeight());
        float scale = Math.Min(mode.X/width, mode.Y/height);

        Love.Graphics.Translate(
            (mode.X - width * scale) * 0.5f,
            (mode.Y - height * scale) * 0.5f
        );
        Love.Graphics.Scale(new Love.Vector2(scale, scale));

        if (centered)
            Love.Graphics.Translate(0.5f * width, 0.5f * height);
        
        return scale;
    }

    public void EndRendering()
    {
        if (!isRendering)
            throw new Exception("Must call BeginRendering before calling EndRendering");
        
        float width = currentlyRendering[0];
        float height = currentlyRendering[1];
        isRendering = false;
        Love.Graphics.Pop();

        Love.Vector2 mode = new Love.Vector2(Love.Graphics.GetWidth(), Love.Graphics.GetHeight());
        float scale = Math.Min(mode.X/width, mode.Y/height);
        width = width * scale;
        height = height * scale;

        Love.Graphics.SetColor(_black);
        Love.Graphics.Rectangle(Love.DrawMode.Fill, 0, 0, mode.X, 0.5f * (mode.Y - height));
        Love.Graphics.Rectangle(Love.DrawMode.Fill, 0, mode.Y, mode.X, 0.5f * (mode.Y - height));
        Love.Graphics.Rectangle(Love.DrawMode.Fill, 0, 0, 0.5f * (mode.X - width), mode.Y);
        Love.Graphics.Rectangle(Love.DrawMode.Fill, mode.X, 0, -0.5f * (mode.X - width), mode.Y);
    }
}