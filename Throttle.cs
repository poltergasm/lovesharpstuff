using System;

namespace Utils
{
public class Throttle
{
    private float min_dt = 1f/60f;
    private float nextTime;
    private float currTime = 0f;

    public Throttle()
    {
        nextTime = (float)Love.Timer.GetTime();
    } 

    public void Update()
    {
        nextTime = nextTime + min_dt;
    }

    public void Draw()
    {
        currTime = (float)Love.Timer.GetTime();
        if (nextTime <= currTime) {
            nextTime = currTime;
        }
        
		Love.Timer.Sleep(nextTime - currTime);
    }
}
}